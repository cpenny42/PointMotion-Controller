#include <gst/gst.h>
#include <gst/rtsp-server/rtsp-server.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


int main (int argc, char *argv[])
{





    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    char host[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1) 
    {
        perror("getifaddrs");
        strcpy(host, "<IP-OF-UDOO>");
    }


    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) 
    {
        if (ifa->ifa_addr == NULL)
            continue;  

        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in),host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);

        if((strcmp(ifa->ifa_name,"wlan0")==0)&&(ifa->ifa_addr->sa_family==AF_INET))
        {
            if (s != 0)
            {
                printf("getnameinfo() failed: %s\n", gai_strerror(s));
                strcpy(host, "<IP-OF-UDOO>");
            }
 //           printf("\tInterface : <%s>\n",ifa->ifa_name );
 //           printf("\t  Address : <%s>\n", host); 
	    break;
        }
    }

    freeifaddrs(ifaddr);








  fprintf(stderr, "Launching Point Motion Controller...\n");
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMediaMapping *mapping;
  GstRTSPMediaFactory *factory;

  gst_init (&argc, &argv);
  loop = g_main_loop_new (NULL, FALSE);
  fprintf(stderr, "    creating rtsp server...\n");
  server = gst_rtsp_server_new ();
  mapping = gst_rtsp_server_get_media_mapping (server);
  factory = gst_rtsp_media_factory_new ();
  fprintf(stderr, "    launching media factory:\n");
  fprintf(stderr, "            ( imxv4l2videosrc capture-mode=0 is-live=1 ! vpuenc codec=0 ! rtpmp4vpay send-config=true name=pay0 pt=96 )\n");
  gst_rtsp_media_factory_set_launch (factory,
       "( imxv4l2videosrc capture-mode=0 is-live=1 ! vpuenc codec=0 ! rtpmp4vpay send-config=true name=pay0 pt=96 )");

  
  gst_rtsp_media_factory_set_shared (factory, TRUE);
  gst_rtsp_media_mapping_add_factory (mapping, "/test", factory);
  g_object_unref (mapping);
  gst_rtsp_server_attach (server, NULL);
  fprintf(stderr, "\nPoint Motion Controller is ready!\nReceive the video stream at:\n     rtsp://%s:8554/test\n", host);
  g_main_loop_run (loop);

  return 0;
}
